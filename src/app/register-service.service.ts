import { Injectable } from '@angular/core';
import { RegisterServiceModel } from './register-service.model';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class RegisterService {

  private registerUrl = 'api/register'; 
  
  constructor(private http: HttpClient) { }

  getHours() : Observable<RegisterServiceModel[]> {
  	return this.http.get<RegisterServiceModel[]>(this.registerUrl);
  }

  addHour(hour: RegisterServiceModel){
  	console.log("Post:");
  	console.log(hour);
    this.http.post(this.registerUrl, hour, httpOptions).subscribe();
  }

}
