import { Component, OnInit, Input } from '@angular/core';
import { NgbDateStruct, NgbTimeStruct } from '@ng-bootstrap/ng-bootstrap';
import { DateTimeDelegate } from './date-time.delegate';

@Component({
  selector: 'app-date-time',
  templateUrl: './date-time.component.html',
  styleUrls: ['./date-time.component.css']
})
export class DateTimeComponent implements OnInit {

	@Input() title : string;
	@Input() delegate: DateTimeDelegate;

  	constructor() { }

  	ngOnInit() { }

}
