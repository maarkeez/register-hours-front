import { NgbDateStruct, NgbTimeStruct } from '@ng-bootstrap/ng-bootstrap';

export class DateTimeDelegate{
	constructor(public date: NgbDateStruct,	public time: NgbTimeStruct) { }

	toDate() : Date {
		return new Date(this.date.year, this.date.month, this.date.day, this.time.hour, this.time.minute );
	}
}