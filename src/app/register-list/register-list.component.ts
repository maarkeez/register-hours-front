import { Component, OnInit } from '@angular/core';
import { RegisterService } from '../register-service.service';
import { RegisterServiceModel } from '../register-service.model';


@Component({
  selector: 'app-register-list',
  templateUrl: './register-list.component.html',
  styleUrls: ['./register-list.component.css']
})
export class RegisterListComponent implements OnInit {

  hours : RegisterServiceModel[];

  constructor(private registerService: RegisterService) { }

  ngOnInit() {
    this.getHours();
  }

  getHours(): void {
    this.registerService.getHours()
    .subscribe(hours => this.hours = hours);
}

}
