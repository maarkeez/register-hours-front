import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddRegisterComponent } from './add-register/add-register.component';
import { RegisterListComponent } from './register-list/register-list.component';

const routes: Routes = [
    { path: '', redirectTo: '/register/add', pathMatch: 'full' },
    { path: 'register/add', component: AddRegisterComponent },
    { path: 'register/list', component: RegisterListComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
