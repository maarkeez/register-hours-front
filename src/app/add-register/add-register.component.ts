import { Component, OnInit } from '@angular/core';
import { DateTimeDelegate } from '../date-time/date-time.delegate';
import { RegisterService } from '../register-service.service';
import { RegisterServiceModel } from '../register-service.model';

@Component({
  selector: 'app-add-register',
  templateUrl: './add-register.component.html',
  styleUrls: ['./add-register.component.css']
})
export class AddRegisterComponent implements OnInit {

  startDateTime = new DateTimeDelegate(null,null);
  endDateTime = new DateTimeDelegate(null,null);

  constructor(private registerService: RegisterService) { }

  ngOnInit() { }

  save(){
  	this.registerService.addHour(
  		new RegisterServiceModel(this.startDateTime.toDate(), this.endDateTime.toDate())
  	);
  }

}
